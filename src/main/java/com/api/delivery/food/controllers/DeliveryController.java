package com.api.delivery.food.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.api.delivery.food.exceptions.ResourceNotFoundException;
import com.api.delivery.food.models.Order;
import com.api.delivery.food.servicesImpl.DeliveryServiceImpl;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/delivery")
public class DeliveryController {
	
	@Autowired
	private DeliveryServiceImpl deliveryServiceImpl;
	
	@GetMapping("/orders")
	public List<Order> getAllOrders(){
		return deliveryServiceImpl.findAll();
	}
	
	@GetMapping("/orders/{id}")
	public ResponseEntity<Order> getOrderById(@PathVariable(value = "id") Long orderId) 
	throws ResourceNotFoundException {
		
		Order order = deliveryServiceImpl.findById(orderId)
				.orElseThrow(() -> new ResourceNotFoundException("Pedido nao encontrado para este id "
						+ orderId));
		return ResponseEntity.ok().body(order);
		
	}
	
	@PostMapping
	public Order createOrder(@Valid @RequestBody Order order) {
		return deliveryServiceImpl.save(order);
	}
	
	@PutMapping("/orders/{id}")
	public ResponseEntity<Order> updateOrder(@PathVariable(value = "id") Long orderId,
			@Valid @RequestBody Order orderDetails) throws ResourceNotFoundException {
		Order order = deliveryServiceImpl.findById(orderId)
				.orElseThrow(() -> new ResourceNotFoundException("Pedido nao encontrado para este id " +
		orderId));
		
		order.setAddress(orderDetails.getAddress());
		order.setName(orderDetails.getName());
		order.setMenu(orderDetails.getMenu());
		
		final Order updatedOrder = deliveryServiceImpl.save(order);
		return ResponseEntity.ok(updatedOrder);
		
	}
	
	@DeleteMapping("/orders/{id}")
	public Map<String, Boolean> deleteOrder(@PathVariable(value = "id") Long orderId)
			throws ResourceNotFoundException {
		Order order = deliveryServiceImpl.findById(orderId)
				.orElseThrow(() -> new ResourceNotFoundException("Pedido nao encontrado para este id "
						+ orderId));
		deliveryServiceImpl.delete(order);
		Map<String, Boolean> response = new HashMap<>();
		response.put("Deletado", Boolean.TRUE);
		return response;
	}
	
	@DeleteMapping("/orders")
	public Map<String, Boolean> deleteAllOrders() throws ResourceNotFoundException{
		deliveryServiceImpl.deleteAll();
		List<Order> orders = deliveryServiceImpl.findAll();
		if(orders == null) {
			new ResourceNotFoundException("N�o h� nenhum pedido cadastrado");
		}
		Map<String, Boolean> response = new HashMap<>();
		response.put("Todos pedidos deletados", Boolean.TRUE);
		return response;
	}

}
