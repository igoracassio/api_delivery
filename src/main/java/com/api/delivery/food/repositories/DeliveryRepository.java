package com.api.delivery.food.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.api.delivery.food.models.Order;

@Repository
public interface DeliveryRepository extends JpaRepository<Order, Long>{

}
