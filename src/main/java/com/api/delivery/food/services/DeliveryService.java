package com.api.delivery.food.services;

import java.util.List;
import java.util.Optional;

import com.api.delivery.food.models.Order;
import com.api.delivery.food.repositories.DeliveryRepository;

public interface DeliveryService{
	
	public List<Order> findAll();
	
	public void deleteAll();
	
	public Optional<Order> findById(Long id);
	
	public Order save(Order order);
	
	public Order update(Long id);
	
	public void delete(Order order);
}
