package com.api.delivery.food.servicesImpl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.api.delivery.food.models.Order;
import com.api.delivery.food.repositories.DeliveryRepository;
import com.api.delivery.food.services.DeliveryService;

@Service
public class DeliveryServiceImpl implements DeliveryService{
	
	@Autowired
	private DeliveryRepository deliveryRepository;

	@Override
	public List<Order> findAll() {
		return this.deliveryRepository.findAll();
	}

	@Override
	public Optional<Order> findById(Long id) {
		return this.deliveryRepository.findById(id);
	}

	@Override
	public Order save(Order order) {
		return this.deliveryRepository.save(order);
	}

	@Override
	public Order update(Long id) {
		return this.update(id);
	}

	@Override
	public void delete(Order order) {
		this.deliveryRepository.delete(order);
	}

	@Override
	public void deleteAll() {
		this.deliveryRepository.deleteAll();
	}
	
}
